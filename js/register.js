$(document).ready(function () {

    let box = $(".register-head-row");
    let th = box.find("th");
    th.each(function () {
        let widthTh = $(this).width();
        let outerWidthTh = $(this).outerWidth();
        let paddingWidth = outerWidthTh - widthTh;
        let widthTitle =  $(this).find(".sort-title").width();
        let widthIcon = $(this).find(".sorting-list-arrows").outerWidth();
        let sum = widthIcon + widthTitle;
        let trueSize = sum + paddingWidth + 2;
        // console.log(Math.round(sum) + " = " + " - " + widthTitle + " - " + widthIcon);
        $(this).css( "min-width", trueSize);
    })
});
