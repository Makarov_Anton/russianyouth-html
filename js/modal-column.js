$(document).ready(function () {


    let boxBtnModal = $(".modal-column-view");
    let defaultDropDown = boxBtnModal.find(".dropdown-menu.dropdown-column .dropdown-item.column-row")
    let defaultColumn = boxBtnModal.find(".list-group-sortable-handles .list-group-item");
    $(".dropdown-item-empty").hide();


    $(".btn-column-view").on("click", function () {
        boxBtnModal.modal("show");
    });
//-----------------------------------------------------------------------------------
//MODAL
//dropdownClick
    $(".dropdown-menu.dropdown-column").unbind("click").on("click"," .dropdown-item.column-row", dropdownClick)
//columnClick
    $(".modal-column-view").unbind("click").on("click"," .column-remove", columnClick);


// btnSave
    boxBtnModal.find(".save-create").on("click",function () {
        $(".list-group-sortable-handles .list-group-item").each(function () {
            console.log($(this).attr("value"));
        })
    })

//btnSelectAll
    boxBtnModal.find(".modal-select-all").on("click",function () {
        $(".dropdown-menu.dropdown-column .dropdown-item.column-row").each(dropdownClick);
    })

//btnUnselectAll
    boxBtnModal.find(".modal-unselect-all").on("click",function () {
        $(".modal-column-view .column-remove").each(columnClick);
    })

//btnDefault
    boxBtnModal.find(".modal-default").on("click",function () {

        $(".list-group-sortable-handles .list-group-item").remove();
        $(".dropdown-menu.dropdown-column .dropdown-item.column-row").remove();

        $(".list-group-sortable-handles").append(defaultColumn);
        $(".dropdown-menu.dropdown-column").append(defaultDropDown);

    })

//-----------------------------------------------------------------------------------

    sortablePlagin();

    function sortablePlagin() {
        $(".list-group-sortable-handles").sortable({
            placeholderClass: "list-group-item",
            handle: "i"
        });
    }

    function columnClick() {
        let val = $(this).parent().attr("value");
        let text = $(this).parent().find("span").html();
        $(this).parent().remove();
        let clone = '<div class="dropdown-item column-row btn" value="' + val + '">' +
                        text +
                    '</div>'
        $(".dropdown-menu.dropdown-column").append(clone);
        sortablePlagin();
        $(".dropdown-item-empty").hide();
        sortablePlagin();
    }

    function dropdownClick() {
        let val = $(this).attr("value");
        let text = $(this).html();
        $(this).remove();
        let clone = '<li class="list-group-item" draggable="true" value="' + val + '">' +
                        '<i class="fa fa-arrows-alt arrow" aria-hidden="true"></i>' +
                        '<span>' + text + '</span> ' +
                        '<a href="#" class="float-right column-remove">' +
                            '<i class=" far fa-trash-alt remove-column "></i>' +
                        '</a>' +
                    '</li>';
        $(".list-group-sortable-handles").append(clone);
        let dropdownLength = $(".dropdown-menu .dropdown-item.column-row").length;
        if (dropdownLength === 0){
            $(".dropdown-item-empty").show();
        }
        sortablePlagin();
    }
});

