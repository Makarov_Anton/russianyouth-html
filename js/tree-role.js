let category = "";
let nodeId;
let idBase = '';

$(document).ready(function() {
    let box = $(".role-box");
    let treeDataRole = JSON.parse(treeRoleJson);
    let notSelected = false;
    let testArr = [0, 6, 7, 11, 12, 15, 16];
    let testSelected = 6;
    box.find('.btn-checked-all, .btn-unchecked-all, .btn-submit').prop('disabled', true);
    $(":checkbox").each(function() {
        $(this).prop('disabled', true);
    });
    $('#treeRole').treeview({
        data: treeDataRole,
        onNodeSelected : function(event, data) {
            box.find('.btn-unchecked-all, .btn-submit, .btn-checked-all').prop('disabled', false);
            if (!notSelected){
                idBase = data.id;
                nodeId = data.nodeId;
                (data.icon === "fa fa-user") ? category = "user" : category = "group";
                box.find(":checkbox").each(function() {
                    $(this).prop('disabled', false);
                });
                console.log(category + " - " + idBase + " - " + nodeId);
                console.log(treeExpanded());
            }else{
                idBase = data.id;
                nodeId = data.nodeId;
                (data.icon === "fa fa-user") ? category = "user" : category = "group";
                box.find(":checkbox").each(function() {
                    $(this).prop('disabled', false);
                });
                console.log(category + " - " + idBase + " - NOT SELECTED " + nodeId);
                console.log(treeExpanded());
                notSelected = false;
            }

        },
        onNodeUnselected: function () {
            box.find('.btn-unchecked-all, .btn-submit, .btn-checked-all').prop('disabled', true);
            box.find(":checkbox").each(function() {
                $(this).prop('disabled', true);
            });
            uncheckedAll();
        }
    });

    $('#treeRole').treeview('collapseAll',{ silent: true });
    treeExpandedView(testArr, testSelected);


    function treeExpanded() {
        let arr = [];
        $('.list-group-item .tree-expand').each(function () {
            arr.push($(this).parent().data("nodeid"));
        })
        return arr
    }

    function treeExpandedView(arr, selected) {
        $.each(arr, function (key, value) {
            $('#treeRole').treeview('expandNode', [value]);
        })
        if (selected != null){
            notSelected = true;
            $('#treeRole').treeview('selectNode', [selected]);
        }
    }
});
