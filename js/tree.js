$(document).ready(function() {
    let treeData = JSON.parse(treeJson);
    let parentId;
    let nodeId = undefined;
    let nodeText = '', idBase = '';
    let box = $('.btn-treeview-box');
    let notSelected = true;
    let testArr = [0, 1, 15, 20, 21];
    let testSelected = null;
    let expanded = [];
    box.find('.btn-treeview-edit, .btn-treeview-remove, .btn-treeview-new-user').prop('disabled', true);


// -------------------------------------------------------------------------------------
// eventBtnRemove(idBase, nodeText);
    box.find('.btn-treeview-remove').on('click', () => {
        let box = $(".modal-remove");
        box.find('.modal-body h6').text(nodeText).css('color','black');
        box.find(".modal-header, .modal-footer").css('border', 'none');
        box.modal('show');
    });

    $('.modal-remove .save-create').on('click', () => {
        $(".form-remove").submit();
    });

    $(".form-remove").submit(function (event) {

        let nodeRemove =  $('#myTree').treeview('getSiblings', nodeId);
        let i = 0;
        let lvl = true;
        while (nodeRemove.length === 0){
            nodeId = $('#myTree').treeview('getNode', parentId)["nodeId"];
            parentId = $('#myTree').treeview('getNode', parentId)["parentId"];
            nodeRemove = $('#myTree').treeview('getSiblings', nodeId);
            i++;
            lvl = false;
        }


        let resArr = [];
        $.each(nodeRemove, function (key, value) {
            let valNode = value.nodeId;

            if (value.nodeId > nodeId && nodeId !== 0){
                let minus = value.nodeId - nodeId;
                expanded = treeExpanded();
                let index = expanded.indexOf(nodeId);
                if (index >= 0) {
                    expanded.splice(index, 1);
                }

                $.each(expanded, function (key, value) {
                    if (value > nodeId){
                      expanded[key] = value - minus;
                  }
                })
                return false;
            }else if(nodeId === 0){
                expanded = treeExpanded();
                let minus = value.nodeId - nodeId;
                $.each(expanded, function (key, value) {
                    if (value >= valNode){
                        resArr.push(value - minus);
                    }
                })
                expanded = resArr;
                return false;
            }else{
                let minus;
                expanded = treeExpanded();
                if (!lvl){
                    nodeId = nodeId + i;
                    lvl = true
                }
                let index = expanded.indexOf(nodeId);

                if (index >= 0) {
                        minus = expanded[index+1] - expanded[index];
                        expanded.splice(index, 1);

                    $.each(expanded, function (key, value) {
                        if (value > nodeId){

                            expanded[key] = value - minus;
                        }
                    });
                    return false;
                }
            }
        });
        console.log(expanded);
        console.log({"id": idBase});
        // console.log(nodeId);
        event.preventDefault();
    });

// eventBtnEdit(idBase, nodeText);
    box.find('.btn-treeview-edit').on('click', () => {
        let box = $(".modal-edit");
        box.find('.edit-input').val(nodeText);
        box.find(".modal-header, .modal-footer").css('border', 'none');
        box.modal('show');

    });

    $('.modal-edit .save-create').on('click', () => {
        $(".form-edit").submit();
    });

    $(".form-edit").submit(function (event) {
        let action = $(this).attr("action") + idBase;
        $(this).attr("action", action)
        console.log(action);
        console.log(editNameFun(idBase, nodeText));
        console.log(treeExpanded());
        console.log(nodeId);
        event.preventDefault();
    })

// eventBtnSave(idBase);
    box.find('.btn-treeview-create').on('click', () => {
        let box = $(".modal-create");
        box.find(".modal-header, .modal-footer").css('border', 'none');
        box.modal('show');
    });

    $('.modal-create .save-create').on('click', function () {
        $(".form-create").submit();
    });

    $(".form-create").submit(function (event) {

        let expanded = treeExpanded();

        $.each(expanded, function (key, value) {
            if (value > nodeId) {
                expanded[key] = value + 1;
            }
        })

        console.log(expanded);
        console.log(createNameFun(idBase));
        console.log(nodeId);
        event.preventDefault();

    })
// -------------------------------------------------------------------------------------
// Init tree
    $('#myTree').treeview({
        data: treeData,
        onNodeSelected : function(event, data) {
            $('.btn-treeview-edit, .btn-treeview-remove, .btn-treeview-new-user').prop('disabled', false);
            if (!notSelected){
                parentId = data.parentId;
                nodeId = data.nodeId;
                nodeText = data.text;
                idBase = data.id;
                console.log(treeExpanded());
                console.log(idBase);
                console.log(nodeText + " - " + idBase + " - " + nodeId);
            }else{
                parentId = data.parentId;
                nodeId = data.nodeId;
                nodeText = data.text;
                idBase = data.id;
                console.log(nodeText + " - " + idBase + " - " + nodeId);
                console.log(treeExpanded());
                notSelected = false;
            }

        },
        onNodeUnselected: function () {
            $('.btn-treeview-edit, .btn-treeview-remove, .btn-treeview-new-user').prop('disabled', true);
        },
        onNodeExpanded: function(event, data){
        // console.log(data);
        }
    });

    $('#myTree').treeview('collapseAll',{ silent: true });
    treeExpandedView(testArr, testSelected);

// -------------------------------------------------------------------------------------
// Function
    function addslashes(string) {
        return string.replace(/\\/g, '\\\\').
        replace(/\u0008/g, '\\b').
        replace(/\t/g, '\\t').
        replace(/\n/g, '\\n').
        replace(/\f/g, '\\f').
        replace(/\r/g, '\\r').
        replace(/'/g, '\\\'').
        replace(/"/g, '\\"');
    }

    function createNameFun(id) {
        let val = $('.create-input').val();
        if (val === ""){
            val = 'Новая группа';
        }
        $('.create-input').val("");
        let createName = addslashes(val);
        return {id, createName};
    }

    function editNameFun(id) {
        let val = $('.edit-input').val();
        let createName = addslashes(val);
        return {id, createName};
    }

    function treeExpanded() {
        let arr = [];
        $('.list-group-item .tree-expand').each(function () {
            arr.push($(this).parent().data("nodeid"));
        })
        return arr
    }

    function treeExpandedView(arr, selected) {
        $.each(arr, function (key, value) {
            $('#myTree').treeview('expandNode', [value]);
        })
        if (selected != null){
            notSelected = true;
            $('#myTree').treeview('selectNode', [selected]);
        }
    }
});