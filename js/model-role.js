let treeRole = [
    {
        text: "отдел продаж",
        icon: "fa fa-users",
        id: 783,
        nodes: [
            {
                text: "розница",
                icon: "fa fa-users",
                id: 983,
                nodes: [
                    {
                        text: "РОП",
                        icon: "fa fa-users",
                        id: 784,
                        nodes: [
                            {
                                text: "Андрей",
                                icon: "fa fa-user",
                                id: 755,

                            },
                            {
                                text: "Оля",
                                icon: "fa fa-user",
                                id: 999,
                            }
                        ]
                    },
                    {
                        text: "Манагер",
                        icon: "fa fa-users",
                        id: 998,
                    }
                ]
            },
            {
                text: "розница",
                icon: "fa fa-users",
                id: 9831,
                nodes: [
                    {
                        text: "РОП",
                        icon: "fa fa-users",
                        id: 9832,
                        nodes: [
                            {
                                text: "Андрей",
                                icon: "fa fa-user",
                                id: 1983
                            },
                            {
                                text: "Оля",
                                icon: "fa fa-user",
                                id: 9854,
                            }
                        ]
                    },
                    {
                        text: "Манагер",
                        icon: "fa fa-users",
                        id: 98323,
                    }
                ]
            }
        ]
    },
    {
        text: "отдел разработки",
        icon: "fa fa-users",
        id: 983123,
        nodes: [
            {
                text: "администратор",
                icon: "fa fa-users",
                id: 98332,
                nodes: [
                    {
                        text: "администратор БД",
                        icon: "fa fa-users"
                    },
                    {
                        text: "Администратор доступа",
                        icon: "fa fa-users"
                    }
                ]
            },
            {
                text: "программист",
                icon: "fa fa-users",
                nodes: [
                    {
                        text: "js developer",
                        icon: "fa fa-users",
                        nodes: [
                            {
                                text: "middle",
                                icon: "fa fa-users"
                            },
                            {
                                text: "junior",
                                icon: "fa fa-users",
                                nodes: [
                                    {
                                        text: "with contract",
                                        icon: "fa fa-users"
                                    },
                                    {
                                        text: "with out contract",
                                        icon: "fa fa-users"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
];

let treeRoleJson = JSON.stringify(treeRole);
