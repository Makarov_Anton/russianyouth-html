$(document).ready(function() {
    $('.boxes-checkbox').each(function() {
        $(this).checkboxall();
    });



    let box = $('.btn-checkbox');




    box.find('.btn-checked-all').on('click', checkedAll);
    box.find('.btn-unchecked-all').on('click', uncheckedAll);
    box.find('.btn-submit').on('click', getCheckedCheckBoxes);
})

function getCheckedCheckBoxes() {
    let resultValue = { };
    let selectedCheckBoxes = document.querySelectorAll('input.checkbox:checked');
    let checkedValues = Array.from(selectedCheckBoxes).map(cb => cb.value);
    resultValue.checkedValues = checkedValues;
    resultValue.categoryId = idBase;
    resultValue.category = category;
    console.log(resultValue);

    return resultValue;
}

function checkedAll() {
    $(":checkbox").each(function() {
        $(this).prop('checked', true);
    });
}

function uncheckedAll() {
    $(":checkbox").each(function() {
        $(this).prop('checked', false);
    });
}