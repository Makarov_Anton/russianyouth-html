$(document).ready(function () {
    let iteration = 1;
    let boxModal = $(".adv-search-modal");
    $(".adv-search-btn").on("click", function () {
        boxModal.modal("show");
    });

    let boxBtnRow = boxModal.find(".adv-search-row-btn-box");
    let boxRow = boxModal.find(".adv-search-row-box");
    let btnRowAdd = boxModal.find(".adv-search-add");
    let btnRowRemove = boxModal.find(".adv-search-remove");
    let btnRowSave = boxModal.find(".adv-search-save");
    let row = boxModal.find(".adv-search-row");

    boxBtnRow.on("click", ".adv-search-add", rowAdd);
    btnRowRemove.prop('disabled',true);
    boxRow.on("click", ".adv-search-remove", rowRemove);
    btnRowSave.on("click", btnSave);
    function rowAdd() {


        if(iteration === 1){
            $(".adv-search-remove").prop('disabled', false);
        }
        let clone = row.clone(true);
        clone.find(".adv-search-input").val("");
        boxRow.append(clone);
        iteration++;
        if (iteration === 5){
            btnRowAdd.prop('disabled', true);
        }
    }

    function rowRemove() {
        $(this).closest(".adv-search-row").remove();
        iteration--;
        console.log(iteration);
        if (iteration === 4){
            btnRowAdd.prop('disabled', false);
        }
        if (iteration === 1){
            $(".adv-search-remove").prop('disabled', true);
        }
    }

    function btnSave() {
        let saveArr = [];
        let val = $(".adv-search-row-box .adv-search-select option:selected");
        let text = $(".adv-search-row-box .adv-search-input");
        $.each(val, function (key, val) {
            let saveObj = {};
            saveObj[val.value] = text[key].value;
            saveArr.push(saveObj)
        })
        console.log(saveArr);
    }


});
