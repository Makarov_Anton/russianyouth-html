$(document).ready(function () {

    // let registerSortCounter = [];
    let headBox = $(".register-head-row");
    let iconHeadBox = $(".register-head-row .sorting-list-arrows");
    iconHeadBox.find("").hide();

//header table
    headBox.find("th").unbind("click").click(headRow);

//----------------------------------------------------------------------------------------------------------------------
    let registerSortCounter = [
        {"email": "asc"},
        {"id": "desc"},
        {"login": "desc"},
        {"group": "desc"}
    ];
    headBox.find("th").each(function () {
        let box = $(this);
        let dataName = box.data("column-name");
        $.each(registerSortCounter, function (key, val) {
            let keyObj = Object.keys(val)[0];
            let valObj = val[keyObj];
            if (valObj === "asc" && keyObj === dataName){
                box.unbind("click", headRow);
                box.find(".register-sort-counter").removeClass("invisible").text(key + 1);
                registerViewAsc(box, false);
            }else if(valObj === "desc" && keyObj === dataName){
                box.unbind("click", headRow);
                box.find(".register-sort-counter").removeClass("invisible").text(key + 1);
                registerViewAsc(box,false);
                registerViewDesk(box.find(".fa-long-arrow-alt-up"), false)
            }
        })
    })

//----------------------------------------------------------------------------------------------------------------------

//arrow UP
    headBox.find(".fa-long-arrow-alt-up").click(function (event) {
        event.stopPropagation();
        let box = $(this);
        let dataName = box.parents("th").data("column-name");
        $.each(registerSortCounter, function (index, value) {
            if (Object.keys(value)[0] === dataName) {
                value[dataName] = "desc";
            }
        });
        box.find(".register-sort-counter").removeClass("invisible").text(registerSortCounter.length + 1);

        registerViewDesk(box, true);
    });
//arrow-DOWN
    headBox.find(".fa-long-arrow-alt-down").click(function (event) {
        event.stopPropagation();
        let dataName = $(this).parents("th").data("column-name");
        $.each(registerSortCounter, function (index, value) {
            if (Object.keys(value)[0] === dataName) {
                value[dataName] = "asc";
            }
        });
        $(this).addClass("d-none");
        $(this).siblings(".fa-long-arrow-alt-up").removeClass("d-none");
        console.log(registerSortCounter);
    });
//remove
    headBox.find(".fa-times").click(function (event) {
        event.stopPropagation();
        let indexRemove = -1;
        let dataName = $(this).parents("th").data("column-name");
        $.each(registerSortCounter, function (index, value) {
            if (Object.keys(value)[0] === dataName) {
                indexRemove = index;
            }
        });
        registerSortCounter.splice(indexRemove, 1);
        $('.register-sort-counter').each(function () {

            let num = $(this).text();
            if (num > indexRemove){
                --num;
                $(this).text(num);
            };
        });

        $(this).addClass("invisible");
        $(this).siblings(".fa-times").removeClass("invisible");
        $(this).siblings(".fa-long-arrow-alt-up").addClass("d-none");
        $(this).siblings(".fa-long-arrow-alt-down").addClass("d-none");
        $(this).siblings(".register-icon-sort").show();
        $(this).siblings(".register-sort-counter").addClass("invisible");
        $(this).closest('th').bind("click",headRow);
        console.log(registerSortCounter);
    });

    function registerViewAsc(box, bool) {
        box.find(".register-icon-sort").hide();
        box.find(".fa-times").removeClass("invisible");
        box.find(".fa-long-arrow-alt-up").removeClass("d-none");
        if (bool){
            console.log(registerSortCounter);
        }
    }

    function registerViewDesk(box, bool) {
        box.addClass("d-none");
        box.siblings(".fa-long-arrow-alt-down").removeClass("d-none");
        if (bool){
            console.log(registerSortCounter);
        }


    }

    function headRow() {
        let box = $(this);
        let dataName = box.data("column-name");
        let sortPos = {};
        sortPos[dataName] = "asc";
        box.find(".register-sort-counter").removeClass("invisible").text(registerSortCounter.length + 1);
        registerSortCounter.push(sortPos);
        registerViewAsc(box, true);
        $(this).unbind("click", headRow);

    }
});